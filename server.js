var express = require('express');
var solr = require('solr-client');

var app = express();
var port = 3000;
var server = require('http').createServer(app);
var io = require('socket.io')(server);

var solrClient = solr.createClient('127.0.0.1', '8983', 'demo', '/solr');
var myQuery = solrClient.createQuery();
myQuery.q('*:*').start('0').rows('100');

server.listen(port);
console.log("Starting on port " + port);

io.on('connection', function (socket) {
    console.log("A user has connected...");
    socket.on('disconnect', function () {
        console.log("A user has disconnected...");
    });

    var interval = setInterval(
        function (client, query) {
            client.get('select', query, function (err, obj) {
                if (err) {
                    console.log(err);
                } else {
                    if (true) {
                        //console.log(obj.response.docs);
                        var data = obj.response.docs;
                        socket.emit('message', {
                            message: data
                        });
                    }
                }
            });
        }, 1000, solrClient, myQuery);

});

app.use(express.static(__dirname + '/public/'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + "/public/index.html");
});

app.get('/*', function (req, res) {
    res.sendFile(__dirname + "/public/index.html");
});
