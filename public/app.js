var solrApp = angular.module('solrApp', ['btford.socket-io']);

solrApp.controller('mainCtrl', ['$scope', '$log', 'chatSocket', function ($scope, $log, chatSocket) {

    $scope.messages = [];

    chatSocket.on('message', function (data) {
        if (!angular.equals($scope.messages, data.message)) {
            $scope.messages = data.message;
            $scope.date = new Date();
            $log.log("Updating data - ", $scope.date);
        }
    });

}]);
